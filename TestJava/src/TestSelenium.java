import static org.junit.Assert.assertEquals;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor; 


import static org.junit.Assert.assertTrue;

import java.io.File;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.homepage.HomePage;
import com.productpage.ProductPage;
import com.shoppingcartpge.qatest.ShoppingCartPage;

import CheckPopUpAndKill.CheckPopUpAndKill;
import SearchResultsPage.SearchResultsPage;

public class TestSelenium {
	
	static WebDriver driver;
	static String productName1 = "Instant Pot Duo Plus60 9-in-1 Multi-Use Programmable Pressure Cooker";
	String ShoppingCartUrl = "https://www.williams-sonoma.com/shoppingcart/";
	String ProductPageUrl = "https://www.williams-sonoma.com/products/all-clad-d5-stainless-steel-10-piece-cookware-set/?pkey=ccookware-sets&isx=0.0.400";
	
	public static void main(String[] args) {		
	}
	
	@SuppressWarnings("deprecation")
	@BeforeClass
	public static void initSetup() {
		System.setProperty("webdriver.chrome.driver", "C:\\Users\\vnegreiros\\eclipse-workspace\\TestJava\\libs\\chromedriver.exe");
		ChromeOptions options = new ChromeOptions();
		options.addExtensions(new File("C:\\Users\\vnegreiros\\eclipse-workspace\\TestJava\\libs\\3.5_0.crx"));
		DesiredCapabilities capabilities = new DesiredCapabilities();
		capabilities.setCapability(ChromeOptions.CAPABILITY, options);
		driver = new ChromeDriver(capabilities);
		driver.manage().window().maximize();
		driver.navigate().refresh();
	    System.out.println("Refresh successfully");
	}
	
	@Test
	/*
	 * Questao 1
	 */
	
	public void addProductToCart () {
		ProductPage product = new ProductPage(driver);
		ShoppingCartPage cart = new ShoppingCartPage(driver);
		
		product.navigateTo();
		assertTrue(product.checkElementExistence(product.getAddToCartBtnId()));
		product.addProductToCart();
		product.checkoutFromAddToCart();
		assertEquals(ShoppingCartUrl ,cart.whoAmI());
		String gotproduct = cart.getProductsOnCart();
		System.out.println(gotproduct);
		assertTrue(gotproduct.contains(productName1));
	}
	
	@Test
	/*
	* Questao 2
	*/
	public void seeProductOnQuickLookOverlay() {
		HomePage homepage = new HomePage(driver);
		SearchResultsPage resultspage = new SearchResultsPage(driver);
		CheckPopUpAndKill popup = new CheckPopUpAndKill(driver);
		
		
		homepage.navigateTo();				
		homepage.SearchText(productName1);
		
		if (popup.isDisplayed())
			popup.kill();
		
		assertTrue(driver.findElements(By.id("results-summary")).size() != 0);
		resultspage.selectQuickOverlaLook();
		String productonoverlay = resultspage.checkQuickOverlaLookProduct();
		System.out.println(productonoverlay);
		//assertTrue(productonoverlay.contains(productName1));
	}
	
	@AfterClass
	public static void TearDown() {
		driver.close();
	}
	
}
