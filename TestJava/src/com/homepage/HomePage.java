package com.homepage;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import CheckPopUpAndKill.CheckPopUpAndKill;

public class HomePage {
	
	
	WebDriver driver;
	String HomePageUrl = "https://www.williams-sonoma.com/";
	String SearchBoxId = "search-field";
	String SearcgbtnId = "btnSearch";
	
	
	//Constructor to initialize the webDriver with the browser driver passed by the testcase
	public HomePage(WebDriver driver) {
		this.driver=driver;
	}
	
	public void navigateTo()
	{
		CheckPopUpAndKill popup = new CheckPopUpAndKill(driver);
		WebDriverWait wait = new WebDriverWait(driver, 10);
		
		//Go to the home page
		driver.get(HomePageUrl);
		
		//check if any popup showed up
		if (popup.isDisplayed())
			popup.kill();
		
		wait.until(ExpectedConditions.urlToBe(HomePageUrl));
	}
	
	public void SearchText(String textToSearch) {
		driver.findElement(By.id(SearchBoxId)).sendKeys(textToSearch);
		driver.findElement(By.id(SearcgbtnId)).click();
	}
	

}
