/**
 * 
 */
package com.shoppingcartpge.qatest;

import org.openqa.selenium.JavascriptExecutor; 
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * @author vnegreiros
 *
 *	This class has all the methods and features for dealing with the Shopping cart page
 *  whose url is: https://www.williams-sonoma.com/shoppingcart/
 *  
 */
public class ShoppingCartPage {
	
	WebDriver driver;

	String ShoppingCartUrl = "https://www.williams-sonoma.com/shoppingcart/";
	String HomePageUrl = "https://www.williams-sonoma.com/";
	
	
	//Constructor to initialize the webDriver with the browser driver passed by the testcase
	public ShoppingCartPage(WebDriver driver) {
		this.driver=driver;
	}
	
	//returns the current Url
	public String whoAmI() {
		WebDriverWait wait = new WebDriverWait(driver, 10);
		wait.until(ExpectedConditions.urlToBe(ShoppingCartUrl));
		return driver.getCurrentUrl();
	}
	
	//returns the title of the product added to the shopping cart
	public String getProductsOnCart() { 
		////*[@id="cartForm"]/div[5]/div[2]/div/div[2]/div[1]/a
		//return driver.findElement(By.className("cart-table-row-title")).getAttribute("class");
		return driver.findElement(By.xpath("//*[@id=\"cartForm\"]/div[5]/div[2]/div/div[2]/div[1]/a")).getText();
	}
	

}
