/**
 * 
 */
package com.productpage;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.JavascriptExecutor; 
import javax.swing.Popup;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Sleeper;
import org.openqa.selenium.support.ui.WebDriverWait;

import CheckPopUpAndKill.CheckPopUpAndKill;




/**
 * @author vnegreiros
 *
 *  This class has all the methods and features for dealing with the Product page
 *  whose url is: https://www.williams-sonoma.com/products/hestan-nanobond-essential-pan-with-cleaner/?pkey=csaute-pans&isx=0.0.367
 *  
 */
public class ProductPage {
	
	WebDriver driver;
	
	String checkOutButtonId = "anchor-btn-checkout";
	String addCartButtonId = "primaryGroup_addToCart_0";
	String productPageUrl = "https://www.williams-sonoma.com/products/instant-pot-duo-plus-pressure-cooker/?pkey=celectrics-instant-pot&isx=0.0.300";
	String HomePageUrl = "http://www.williams-sonoma.com/";
	
	
	//Constructor to initialize the webDriver with the browser driver passed by the testcase
	public ProductPage(WebDriver driver) {
		this.driver=driver;
	}
	
	
	//Opens home page, and navigate to the product page by clicking on the products page and then on the product
	public void navigateTo()  {
		
		CheckPopUpAndKill popup = new CheckPopUpAndKill(driver);
		WebDriverWait wait = new WebDriverWait(driver, 10);
		
		//Go to the home page
		driver.get(HomePageUrl);
		
		//check if any popup showed up
		if (popup.isDisplayed())
			popup.kill();
		
		//click on the product category
		wait.until(ExpectedConditions.elementToBeClickable(By.id("imagerollover8056438")));
		driver.findElement(By.id("imagerollover8056438")).click();
		
		//check if any popup showed up
		if (popup.isDisplayed())
			popup.kill();
		
		//click on the product and enter product page
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"subCatListContainer\"]/ul/li[1]")));
		driver.findElement(By.xpath("//*[@id=\"subCatListContainer\"]/ul/li[1]")).click();
		
	}
	
	//returns the current Url
	public String whoAmI() {
		WebDriverWait wait = new WebDriverWait(driver, 10);
		wait.until(ExpectedConditions.urlMatches("productPageUrl"));
		return driver.getCurrentUrl();
	}
	
	//hits the "Add to cart" button, opening an overlay with cart info
	public void addProductToCart() {

		CheckPopUpAndKill popup = new CheckPopUpAndKill(driver);
		WebDriverWait wait = new WebDriverWait(driver, 10);
		
		//check if any popup showed up
		if (popup.isDisplayed())
			popup.kill();
		
		wait.until(ExpectedConditions.elementToBeClickable(By.id(addCartButtonId)));
		driver.findElement(By.id(addCartButtonId)).click();
	}
	
	//This method checks if an element exists using its Id
	public boolean checkElementExistence(String id) {	
		driver.manage().timeouts().implicitlyWait(0, TimeUnit.MILLISECONDS);
		boolean exists = driver.findElements( By.id(id) ).size() != 0;
		driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
		return exists;
	}
	
	//hits the "Checkout" button from Adding to cart overlay
	public void checkoutFromAddToCart() {
		WebDriverWait wait = new WebDriverWait(driver, 10);
		wait.until(ExpectedConditions.elementToBeClickable(By.id(checkOutButtonId)));
		driver.findElement(By.id(checkOutButtonId)).click();
	}
	
	public String getAddToCartBtnId() {
		return this.addCartButtonId;
	}
	

}
