package CheckPopUpAndKill;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class CheckPopUpAndKill {
	
	WebDriver driver;
	
	public CheckPopUpAndKill(WebDriver driver) {
		this.driver=driver;
	}
	
	public boolean isDisplayed() {
		
		try {
			Thread.sleep(10000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		driver.manage().timeouts().implicitlyWait(0, TimeUnit.MILLISECONDS);
		boolean exists = (driver.findElements(By.className("stickyOverlayWidget")).size() != 0);
		driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);

		return exists;
	}
	
	public void kill() {
		WebDriverWait wait = new WebDriverWait(driver, 10);
		//Actions action = new Actions(driver);
		//action.sendKeys(Keys.ESCAPE);

		wait.until(ExpectedConditions.elementToBeClickable(By.className("stickyOverlayMinimizeButton")));
		
		//minimize the big popup
		driver.findElement(By.className("stickyOverlayMinimizeButton")).click();
		
		//wait two seconds
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		//close the popup
		////*[@id="shop"]/div[9]/div/a[3] - xpath close button
		driver.findElement(By.className("stickyHeaderCloseButton")).click();

		System.out.println("Popup killed");
	}
	

}
