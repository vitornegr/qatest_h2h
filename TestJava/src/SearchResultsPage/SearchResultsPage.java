package SearchResultsPage;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class SearchResultsPage {
	
	WebDriver driver;	
	
	//Constructor to initialize the webDriver with the browser driver passed by the testcase
	public SearchResultsPage(WebDriver driver) {
		this.driver=driver;
	}
	
	public void selectQuickOverlaLook()
	{
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
			//driver.findElement(By.xpath("//*[@id=\"search-tiles\"]/ul/li[1]/div[1]/div/a/img")).click();
		driver.findElement(By.xpath("//*[@id=\"search-tiles\"]/ul/li[1]/div[2]/quick-look/div/a/span")).click();
	}
	
	public String checkQuickOverlaLookProduct()
	{
		////*[@id="purchasing-container"]/div[2]/div/h1
		return driver.findElement(By.xpath("//*[@id=\"purchasing-container\"]/div[2]/div/h1")).getText();
	}

}
